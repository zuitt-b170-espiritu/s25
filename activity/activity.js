
// Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate([
   { $match: { onSale: true } },
   { $group: { _id: null, fruitsonSale: { $sum: 1 } } },
   { $project: { _id: 0 } }
]);
   
   

//  Use the count operator to count the total number of fruits with stock more than 20
db.fruits.aggregate([
   { $match: { stock:{$gt:20}} },
   { $group: { _id: null, enoughStock: { $sum: 1 } } },
   { $project: { _id: 0 } }
]);
   

// Use the average operator to get the average price of fruits onSale per supplier.
db.fruits.aggregate([
   { $match: { onSale: true } },
   { $group: { _id: "$supplier_id", avePrice: { $avg: "$price" } } }
]);


// Use the max operator to get the highest price of a fruit per supplier
db.fruits.aggregate([
   
   { $group: { _id: "$supplier_id", maxPrice: { $max: "$price" } } }
]);


// Use the min operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
   
   { $group: { _id: "$supplier_id", minPrice: { $min: "$price" } } }
]);