/*
Mini Activity
	add the following documents for the "fruits" collection (follow the format that you have made last session)
		FRUITS: can be changed depending on your preference
		apple
		mango
		banana
		orange

		FORMAT/FIELDS
		name:
		color:
		stock:
		price:
		supplier_id:
		onSale:
		origin:[atleast 2 country origins]


*/


db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {




db.fruits.aggregate([
    {$match: {onSale: true}}
])



db.fruits.aggregate([
    {$match: { onSale:true}}
   ])